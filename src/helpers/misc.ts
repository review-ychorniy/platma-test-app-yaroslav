import { OpenAPIV3 } from "openapi-types";
import { get } from "radash";

import {
  isOperationObject,
  isRequestBodyObject,
  Operation,
  SchemaDetails,
} from "@/types/misc";

export function parseSchemasDetails(apiDocument: OpenAPIV3.Document) {
  const result: {
    [key: string]: SchemaDetails;
  } = {};

  if (apiDocument.components && apiDocument.components.schemas) {
    const pathLists = apiDocument.paths || {};
    const pathList = Object.entries(pathLists);

    const schemas = Object.entries(apiDocument.components?.schemas);
    schemas.forEach(([schema, schemaObject]) => {
      const schemaOperations: Operation[] = [];

      pathList.forEach(([pathName, pathRequests]) => {
        const requests = pathRequests || {};
        const requestsArr = Object.entries(requests);

        requestsArr.forEach(([requestMethod, requestInfo]) => {
          if (isOperationObject(requestInfo)) {
            const tags: string[] = requestInfo.tags || [];
            if (tags.includes(schema.toLowerCase())) {
              schemaOperations.push({
                title: requestInfo.summary || "",
                operationId: requestInfo.operationId || "",
                path: pathName,
                method: requestMethod,
                operationObject: requestInfo,
              });
            }
          }
        });
      });

      result[schema.toLowerCase()] = {
        schemaObject,
        schemaOperations,
      };
    });
  }

  return result;
}

export function getObjectByRefPath(
  apiDocument: OpenAPIV3.Document,
  refPath: string,
) {
  return get(apiDocument, refPath.slice(2).replaceAll("/", "."));
}

export function getApiUrlServer(apiDocument: OpenAPIV3.Document): string {
  const servers = apiDocument.servers || [];
  return get(servers, "[0].url");
}

export function getRequestBodySchemaInfo(
  requestBody:
    | OpenAPIV3.ReferenceObject
    | OpenAPIV3.RequestBodyObject
    | undefined,
): {
  schema: OpenAPIV3.SchemaObject | OpenAPIV3.ReferenceObject;
  contentType: string;
} {
  let schema: any;
  let contentType = "";

  if (isRequestBodyObject(requestBody)) {
    const contentTypes = Object.keys(requestBody.content);
    contentType = contentTypes.length ? contentTypes[0] : "";

    if (contentType) {
      schema = requestBody.content[contentType].schema;
    }
  }

  return {
    schema,
    contentType,
  };
}
