import { OpenAPIV3 } from "openapi-types";

export type SchemaObjectType =
  | OpenAPIV3.NonArraySchemaObjectType
  | OpenAPIV3.ArraySchemaObjectType;

export function isReferenceObject(
  object: any,
): object is OpenAPIV3.ReferenceObject {
  return object && typeof object === "object" && "$ref" in object;
}

export function isSchemaObject(object: any): object is OpenAPIV3.SchemaObject {
  return !isReferenceObject(object);
}

export function isOperationObject(
  object: any,
): object is OpenAPIV3.OperationObject {
  return object && typeof object === "object" && "tags" in object;
}

export function isRequestBodyObject(
  object: any,
): object is OpenAPIV3.RequestBodyObject {
  return object && typeof object === "object" && "content" in object;
}

export type Operation = {
  title: string;
  operationId: string;
  path: string;
  method: string;
  operationObject: OpenAPIV3.OperationObject;
};

export type SchemaDetails = {
  schemaObject: OpenAPIV3.ReferenceObject | OpenAPIV3.SchemaObject;
  schemaOperations: Operation[];
};
