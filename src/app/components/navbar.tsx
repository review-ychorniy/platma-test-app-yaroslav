import { Button, Flex, Stack, Title } from "@mantine/core";

import { useSchemasDetails } from "@/store/useSchemaDetails";
import { useSchemas } from "@/store/useSchemas";

export function Navbar() {
  const { schemas, activeSchema, setActiveSchema } = useSchemas();
  const { getOperationsLength } = useSchemasDetails();

  return (
    <>
      <Flex justify="center">
        <Title order={3}>Schemas</Title>
      </Flex>
      <Stack mt="md">
        {schemas
          // .sort((a, b) => {
          //   return (
          //     getOperationsLength(b.toLowerCase()) -
          //     getOperationsLength(a.toLowerCase())
          //   );
          // })
          .map((schema) => {
            return (
              <Button
                key={schema}
                variant={schema === activeSchema ? "filled" : "default"}
                onClick={() => setActiveSchema(schema)}
              >
                {schema}
              </Button>
            );
          })}
      </Stack>
    </>
  );
}
