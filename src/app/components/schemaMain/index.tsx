import { Box, Card, Flex, Mark, Title } from "@mantine/core";

import { useSchemaDetails } from "@/store/useSchemaDetails";
import { useSchemas } from "@/store/useSchemas";

import { SchemaObject } from "./schemaObject";
import { SchemaOperations } from "./schemaOperations";

export function SchemaMain() {
  const { activeSchema } = useSchemas();
  const { schemaObject, schemaOperations } = useSchemaDetails(
    activeSchema?.toLowerCase() || "",
  );

  if (!schemaObject) {
    return <Box>Schema is empty</Box>;
  }

  return (
    <>
      <Box maw="30rem" m="auto">
        <Card withBorder shadow="sm" radius="md">
          <Card.Section withBorder inheritPadding py="xs">
            <Flex justify="center">
              <Title order={3}>
                Schema -{" "}
                <Mark color="orange" px="0.2rem" py="0.1rem">
                  {activeSchema}
                </Mark>
              </Title>
            </Flex>
          </Card.Section>
          <SchemaObject
            keyName={activeSchema || ""}
            schema={schemaObject}
            isLast={false}
          />
        </Card>
      </Box>
      <Box>
        <SchemaOperations operations={schemaOperations} />
      </Box>
    </>
  );
}
