import { Box, Button, Flex, Group, Title } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { useState } from "react";

import { Operation } from "@/types/misc";

import { SchemaOperationsCall } from "./schemaOperationCall";

type Props = {
  operations: Operation[];
};

export function SchemaOperations({ operations }: Props) {
  const [opened, { open, close }] = useDisclosure(false);
  const [operation, setOperation] = useState<Operation | null>(null);

  if (!operations.length) {
    return null;
  }

  const handleClick = (operation: Operation) => () => {
    setOperation(operation);
    open();
  };

  return (
    <>
      {operation && (
        <SchemaOperationsCall
          opened={opened}
          operation={operation}
          onClose={close}
        />
      )}
      <Box p="md" maw="50rem" m="auto">
        <Flex align="center" justify="center">
          <Title mb="md">Operations List</Title>
        </Flex>
        <Group>
          {operations.map((operation) => {
            return (
              <Button
                key={operation.operationId}
                onClick={handleClick(operation)}
              >
                {operation.title}
              </Button>
            );
          })}
        </Group>
      </Box>
    </>
  );
}
