import {
  Box,
  Button,
  Flex,
  Modal,
  NumberInput,
  SimpleGrid,
  Text,
  TextInput,
} from "@mantine/core";
import { OpenAPIV3 } from "openapi-types";
import { useState } from "react";
import {
  FormProvider,
  SubmitHandler,
  useForm,
  useFormContext,
} from "react-hook-form";

import {
  getApiUrlServer,
  getObjectByRefPath,
  getRequestBodySchemaInfo,
} from "@/helpers/misc";
import { useApiDocumentData } from "@/store/useApiDocumentData";
import { isReferenceObject, isSchemaObject, Operation } from "@/types/misc";

type Props = {
  opened: boolean;
  operation: Operation;
  onClose: () => void;
};

function RequestBodyField({
  keyName,
  schema,
}: {
  keyName: string;
  schema: OpenAPIV3.ReferenceObject | OpenAPIV3.SchemaObject;
}) {
  const methods = useFormContext();
  const { documentData } = useApiDocumentData();
  if (documentData && isReferenceObject(schema)) {
    schema = getObjectByRefPath(
      documentData,
      schema.$ref,
    ) as OpenAPIV3.SchemaObject;
  }

  if (!isSchemaObject(schema)) {
    return null;
  }

  let el;

  switch (schema.type) {
    case "object": {
      const properties = Object.entries(schema.properties || {});
      el = properties.map(([propertyKey, propertyInfo]) => {
        return (
          <RequestBodyField
            key={propertyKey}
            keyName={propertyKey}
            schema={propertyInfo}
          />
        );
      });

      break;
    }
    case "integer":
    case "number": {
      el = (
        <NumberInput
          placeholder={keyName}
          onChange={(val) => {
            methods.setValue(`${keyName}`, val);
          }}
        />
      );
      break;
    }
    case "string": {
      el = (
        <TextInput
          placeholder={keyName}
          onChange={(ev) => {
            methods.setValue(`${keyName}`, ev.target.value);
          }}
        />
      );
      break;
    }
    default: {
      break;
    }
  }

  return el;
}

export function SchemaOperationsCall({ opened, operation, onClose }: Props) {
  const { operationObject } = operation;
  const { requestBody, parameters } = operationObject;
  const { documentData } = useApiDocumentData();
  const [result, setResult] = useState("");

  const { schema: requestBodySchema, contentType } =
    getRequestBodySchemaInfo(requestBody);
  const methods = useForm<any>();
  const onSubmit: SubmitHandler<any> = (data) => {
    setResult("");

    if (documentData) {
      const serverUrl = getApiUrlServer(documentData);
      const method = operation.method.toLowerCase();

      if (method === "get") {
        fetch(serverUrl + operation.path)
          .then((resp) => resp.json())
          .then((data) => {
            setResult(data);
          })
          .catch((err) => {
            setResult(err);
          });
      }

      if (operation.method.toLowerCase() !== "get") {
        fetch(operation.path, {
          method: operation.method,
          headers: {
            "content-type": contentType,
          },
          body: JSON.stringify(data),
        })
          .then((resp) => resp.json())
          .then((data) => {
            setResult(data);
          })
          .catch((err) => {
            setResult(err);
          });
      }
    }
  };

  return (
    <Modal
      opened={opened}
      onClose={() => {
        setResult("");
        onClose();
      }}
      title={operation.title}
    >
      <FormProvider {...methods}>
        <form noValidate onSubmit={methods.handleSubmit(onSubmit)}>
          <SimpleGrid cols={2} spacing="sm" verticalSpacing="lg">
            {requestBodySchema && (
              <RequestBodyField keyName="" schema={requestBodySchema} />
            )}
          </SimpleGrid>
          <Flex justify="center" mt="md">
            <Button type="submit">Send Request</Button>
          </Flex>
          {result && (
            <Box>
              <pre>
                <Text c="grape">{JSON.stringify(result, null, 2)}</Text>
              </pre>
            </Box>
          )}
        </form>
      </FormProvider>
    </Modal>
  );
}
