import { Badge, Box, Card, Flex, Text } from "@mantine/core";
import { OpenAPIV3 } from "openapi-types";

import { isReferenceObject, SchemaObjectType } from "@/types/misc";

type Props = {
  isLast: boolean;
  keyName: string;
  schema: OpenAPIV3.ReferenceObject | OpenAPIV3.SchemaObject;
};

export function SchemaType({
  keyName,
  type,
}: {
  keyName: string;
  type: SchemaObjectType;
}) {
  return (
    <Flex align="center">
      <Text fz={16} fw={500}>
        {keyName}
      </Text>
      <Badge ml="0.2rem">{type}</Badge>
    </Flex>
  );
}

export function SchemaObject({ keyName, schema, isLast }: Props) {
  if (isReferenceObject(schema)) {
    return <Box>not implemented</Box>;
  }

  let el;

  switch (schema.type) {
    case "object": {
      const properties = Object.entries(schema.properties || []);

      return properties.map(([key, value]) => {
        return (
          <SchemaObject
            key={key}
            keyName={key}
            schema={value}
            isLast={properties.at(-1)?.[0] === key}
          />
        );
      });
    }
    case "integer":
    case "number":
    case "boolean":
    case "string":
    case "array": {
      el = <SchemaType keyName={keyName} type={schema.type} />;
      break;
    }
    default: {
      break;
    }
  }

  return (
    <Card.Section withBorder={!isLast} inheritPadding py="xs">
      {el}
    </Card.Section>
  );
}
