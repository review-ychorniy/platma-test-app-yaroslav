import { Box, Button, Center, Flex, Input } from "@mantine/core";

import { useApiUrl } from "@/store/useApiUrl";

type Props = {
  onBtnClick: () => void;
};

export function Header({ onBtnClick }: Props) {
  const { apiUrl, setApiUrl } = useApiUrl();

  return (
    <Center h="100%" p="xl">
      <Flex w="100%" my="xl">
        <Input
          w="100%"
          placeholder="Open Api Url"
          value={apiUrl}
          onChange={(ev) => {
            setApiUrl(ev.target.value);
          }}
        />
        <Box ml="1rem">
          <Button pl="md" onClick={onBtnClick}>
            Refetch
          </Button>
        </Box>
      </Flex>
    </Center>
  );
}
