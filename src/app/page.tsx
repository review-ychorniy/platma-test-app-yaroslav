"use client";
import { AppShell, Burger, LoadingOverlay } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { useEffect } from "react";

import { useOpenApiDocument } from "@/requests/useOpenApiDocument";
import { useApiDocumentData } from "@/store/useApiDocumentData";
import { useApiUrl } from "@/store/useApiUrl";
import { useSchemasDetails } from "@/store/useSchemaDetails";
import { useSchemas } from "@/store/useSchemas";

import { Header } from "./components/header";
import { Navbar } from "./components/navbar";
import { SchemaMain } from "./components/schemaMain";

export default function Home() {
  const [opened, { toggle }] = useDisclosure();
  const { apiUrl } = useApiUrl();
  const { isLoading, isRefetching, data, refetch } = useOpenApiDocument(
    apiUrl,
    {
      refetchOnWindowFocus: false,
    },
  );
  const { setDocumentData } = useApiDocumentData();
  const { setShemasFromApiDocument } = useSchemas();
  const { setSchemasDetails } = useSchemasDetails();

  useEffect(() => {
    if (data) {
      setDocumentData(data);
      setShemasFromApiDocument(data);
      setSchemasDetails(data);
    }
  }, [data, setDocumentData, setSchemasDetails, setShemasFromApiDocument]);

  return (
    <AppShell
      header={{ height: 80 }}
      navbar={{
        width: 300,
        breakpoint: "sm",
        collapsed: { mobile: !opened },
      }}
      padding="md"
    >
      <AppShell.Header>
        <Burger opened={opened} onClick={toggle} hiddenFrom="sm" size="sm" />

        <Header onBtnClick={refetch} />
      </AppShell.Header>

      <AppShell.Navbar p="md">
        <Navbar />
      </AppShell.Navbar>

      <AppShell.Main>
        <SchemaMain />
      </AppShell.Main>
      <LoadingOverlay visible={isLoading || isRefetching} />
    </AppShell>
  );
}
