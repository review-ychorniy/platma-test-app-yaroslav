import { useQuery, UseQueryOptions } from "@tanstack/react-query";
import ky from "ky";
import { OpenAPIV3 } from "openapi-types";

export async function getOpenApiDocument(
  apiUrl: string,
): Promise<OpenAPIV3.Document> {
  return ky.get(apiUrl).json();
}

export function useOpenApiDocument(
  apiUrl: string,
  options?: Partial<
    UseQueryOptions<OpenAPIV3.Document, any, OpenAPIV3.Document, any>
  >,
) {
  return useQuery({
    queryKey: ["openApiDocument", apiUrl],
    queryFn: () => getOpenApiDocument(apiUrl),
    ...options,
  });
}
