import { OpenAPIV3 } from "openapi-types";
import { useMemo } from "react";
import { create } from "zustand";

import { parseSchemasDetails } from "@/helpers/misc";
import { SchemaDetails } from "@/types/misc";

type State = {
  schemasDetails: {
    [key: string]: SchemaDetails;
  };
  setSchemasDetails: (apiDocuments: OpenAPIV3.Document) => void;
  getSchemaDetails: (schemaKey: string) => SchemaDetails;
  getOperationsLength: (schemaKey: string) => number;
};

export const useSchemasDetails = create<State>()((set, get) => ({
  schemasDetails: {},
  setSchemasDetails: (apiDocuments) => {
    set({
      schemasDetails: parseSchemasDetails(apiDocuments),
    });
  },
  getSchemaDetails: (schemaKey) => {
    return (
      get().schemasDetails[schemaKey] || {
        schemaOperations: [],
      }
    );
  },
  getOperationsLength: (schemaKey) => {
    return get().getSchemaDetails(schemaKey).schemaOperations.length;
  },
}));

export const useSchemaDetails = (schemaKey: string) => {
  const { getSchemaDetails } = useSchemasDetails();

  return useMemo(() => {
    return getSchemaDetails(schemaKey);
  }, [getSchemaDetails, schemaKey]);
};
