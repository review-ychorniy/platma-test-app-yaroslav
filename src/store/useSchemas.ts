import { OpenAPIV3 } from "openapi-types";
import { create } from "zustand";

type State = {
  schemas: string[];
  activeSchema: string | null;
  setSchemas: (schemas: string[]) => void;
  setActiveSchema: (activeSchema: string | null) => void;
  setShemasFromApiDocument: (apiDocument: OpenAPIV3.Document) => {
    schemas: string[];
    activeSchema: string | null;
  };
};

export const useSchemas = create<State>()((set) => ({
  schemas: [],
  activeSchema: null,
  setSchemas: (schemas) => {
    set({ schemas });
  },
  setActiveSchema: (activeSchema) => {
    set({ activeSchema });
  },
  setShemasFromApiDocument: (apiDocument) => {
    let schemas: string[] = [];
    let activeSchema = null;

    if (apiDocument.components && apiDocument.components?.schemas) {
      schemas = Object.keys(apiDocument.components?.schemas);
      activeSchema = schemas.length ? schemas[0] : null;
      set({ schemas, activeSchema });
    }

    return {
      schemas,
      activeSchema,
    };
  },
}));
