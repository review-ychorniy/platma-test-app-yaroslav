import { OpenAPIV3 } from "openapi-types";
import { create } from "zustand";

type State = {
  documentData: OpenAPIV3.Document | null;
  setDocumentData: (documentData: OpenAPIV3.Document) => void;
};

export const useApiDocumentData = create<State>()((set) => ({
  documentData: null,
  setDocumentData: (documentData) => {
    set({ documentData });
  },
}));
