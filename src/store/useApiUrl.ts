import { create } from "zustand";

const BASE_API_URL =
  "https://raw.githubusercontent.com/readmeio/oas-examples/main/3.0/json/petstore.json";

type State = {
  apiUrl: string;
  setApiUrl: (apiUrl: string) => void;
};

export const useApiUrl = create<State>()((set) => ({
  apiUrl: BASE_API_URL,
  setApiUrl: (apiUrl) => {
    set({ apiUrl });
  },
}));
